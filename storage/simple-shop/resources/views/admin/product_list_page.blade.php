<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Laravel</title>
</head>
@include('admin.menu')
<body>

<h3>商品一覧</h3>

<form action="/admin/product_register_page" method="get">
    <button type="submit" class="btn btn-secondary">追加</button>
</form>

<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">商品名</th>
        <th scope="col">値段</th>
        <th scope="col">詳細</th>
        <th scope="col">編集</th>
        <th scope="col">削除</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($products as $product)
        <tr>
            <td> {{ $product->getId()->get()}} </td>
            <td> {{ $product->getName() }}</td>
            <td> {{ $product->getPrice()}}</td>
            <td> {{ $product->getDescription()}}</td>
            <td>
                <form action="/admin/product_detail_page" method="get">
                    <input type="hidden" name="product_id" value="{{$product->getId()->get()}}" />
                    <button type="submit" class="btn btn-primary">編集</button>
                </form>
            </td>
            <td>
                <form action="/admin/delete_product" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_id" value="{{$product->getId()->get()}}" />
                    <button type="submit" class="btn btn-danger">削除</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>