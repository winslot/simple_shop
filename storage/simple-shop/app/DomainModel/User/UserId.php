<?php namespace App\DomainModel\User;

class UserId
{
	private $id;

	public function __construct(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function get(): int
	{
		return $this->id;
	}
}