<?php namespace App\DomainModel\User;


class UserRepository
{
	/**
	 * @return User
	 * @throws \Exception
	 */
	public function getLoginUser()
	{
		$userModel = \Auth::user();

		if (is_null($userModel)) {
			throw new \Exception("ログイン状態ではないため、ユーザー情報を取得することはできません。");
		}

		return User::reconstruction(
			new UserId($userModel->id),
			$userModel->name,
			$userModel->email
		);
	}
}