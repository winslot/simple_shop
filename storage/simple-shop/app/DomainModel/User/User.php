<?php namespace App\DomainModel\User;

class User
{
	private $userId;

	private $name;

	private $email;

	private function __construct(UserId $id, string $name, string $email)
	{
		$this->userId = $id;
		$this->name = $name;
		$this->email = $email;
	}

	/**
	 * 再構築
	 * @param UserId $id
	 * @param string $name
	 * @param string $email
	 * @return User
	 */
	public static function reconstruction(UserId $id, string $name, string $email)
	{
		return new static($id, $name, $email);
	}

	public function getUserName()
	{
		return $this->name;
	}
}