<?php namespace App\DomainModel\Product;

class ProductId
{
	private $id;

	public function __construct(int $id = null)
	{
		$this->id = $id;
	}

	public function get()
	{
		return $this->id;
	}
}