<?php namespace App\DomainModel\Product;

use App\Model\Product as ProductModel;

class ProductRepository
{
	/**
	 * 検索上限
	 */
	const LIMIT = 100;

	/**
	 * 商品の実体を再構築する
	 * @param ProductId $id
	 * @return Product
	 */
	public function find(ProductId $id)
	{
		$model = ProductModel::find($id->get());

		if (is_null($model)) {
			return null;
		}

		return Product::reconstruction(
			new ProductId($model->id),
			$model->name,
			$model->description,
			$model->price,
			$model->registered_at
		);
	}

	/**
	 * 全部返す
	 * @return Products
	 */
	public function all(): Products
	{
		$models = ProductModel::limit(self::LIMIT)->get();
		$products = $models->map(function (ProductModel $model) {
			return Product::reconstruction(
				new ProductId($model->id),
				$model->name,
				$model->description,
				$model->price,
				$model->registered_at
			);
		})->toBase();

		return new Products($products);
	}

	/**
	 * キーワードで検索する。
	 * メモ：where likeでは、indexが効かないため、恐らく万くらいで限界になると思われます。
	 * @param string $keyword
	 * @return Products
	 */
	public function selectByKeyword(string $keyword)
	{
		$models = ProductModel::where('name', 'like', '%' . $keyword. '%')
			->limit(self::LIMIT)
			->get();

		$products = $models->map(function (ProductModel $model) {
			return Product::reconstruction(
				new ProductId($model->id),
				$model->name,
				$model->description,
				$model->price,
				$model->registered_at
			);
		})->toBase();

		return new Products($products);
	}

	/**
	 * 商品を永続化する
	 * @param Product $product
	 */
	public function persist(Product $product)
	{
		$productId = $product->getId();

		if (is_null($productId->get())) {
			$productModel = new ProductModel();
			$productModel->name = $product->getName();
			$productModel->description = $product->getDescription();
			$productModel->price = $product->getPrice();
			$productModel->registered_at = $product->getRegisteredDatetime();
			$productModel->save();

			$product->loadId(new ProductId($productModel->id));
			return;
		}

		/** @var ProductModel $productModel */
		$productModel = ProductModel::find($productId->get());
		$productModel->name = $product->getName();
		$productModel->description = $product->getDescription();
		$productModel->price = $product->getPrice();
		$productModel->save();
	}

	/**
	 * 商品削除する
	 * @param ProductId $id
	 */
	public function delete(ProductId $id)
	{
		ProductModel::where('id', $id->get())->delete();
	}
}