<?php namespace App\DomainModel\Product;

use Illuminate\Support\Collection;

/**
 * 商品クラスのファーストクラスコレクション
 * Class Products
 * @package App\DomainModel\Product
 */
class Products
{
	private $products;

	/**
	 * Products constructor.
	 * @param Collection|Product[] $products
	 */
	public function __construct(Collection $products)
	{
		$this->products = $products;
	}

	/**
	 * 特に処理をすることなく、そのままコレクションを返す。
	 * @return Collection|Product[]
	 */
	public function getAll(): Collection
	{
		return $this->products;
	}

	/**
	 * 情報を配列として返す。
	 * @return array
	 */
	public function toArray()
	{
		return $this->products->map(function (Product $product) {
			return [
				'id' => $product->getId()->get(),
				'name' => $product->getName(),
				'description' => $product->getDescription(),
				'price' => $product->getPrice(),
				'image_name' =>$product->getImageName()
			];
		})->toArray();
	}
}