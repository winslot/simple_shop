<?php namespace App\DomainModel\Product;

use Carbon\Carbon;

class Product
{
	private $id;

	private $name;

	private $description;

	private $price;

	private $registeredDate;

	private function __construct(ProductId $id, string $name, string $description, int $price, Carbon $registeredDate)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
		$this->price = $price;
		$this->registeredDate = $registeredDate;
	}

	/**
	 * 新しい商品を登録する
	 * @param string $name
	 * @param string $description
	 * @param int $price
	 * @return Product
	 */
	public static function registerProduct(string $name, string $description, int $price)
	{
		return new static(
			new ProductId(null),
			$name,
			$description,
			$price,
			Carbon::now()
		);
	}

	/**
	 * DBから商品を再構築する
	 * @param ProductId $id
	 * @param string $name
	 * @param string $description
	 * @param int $price
	 * @param Carbon $registeredDate
	 * @return Product
	 */
	public static function reconstruction(ProductId $id, string $name, string $description, int $price, Carbon $registeredDate)
	{
		return new static($id, $name, $description, $price, $registeredDate);
	}

	/**
	 * @return ProductId
	 */
	public function getId():ProductId
	{
		return $this->id;
	}

	/**
	 * 初回商品登録・永続化した直後に、DBにより自動生成したIDをロードする。
	 * @param ProductId $id
	 */
	public function loadId(ProductId $id)
	{
		$this->id = $id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function getPrice(): int
	{
		return $this->price;
	}

	public function getRegisteredDatetime(): Carbon
	{
		return $this->registeredDate;
	}

	/**
	 * 商品改名
	 * @param string $name
	 */
	public function rename(string $name)
	{
		$this->name = $name;
	}

	/**
	 * 商品詳細更新
	 * @param string $description
	 */
	public function updateDescription(string $description)
	{
		$this->description = $description;
	}

	/**
	 * 画像名を返す。
	 * @return string
	 * @throws \Exception
	 */
	public function getImageName()
	{
		if (is_null($this->id->get())) {
			throw new \Exception("商品IDはまたIDを所持してないため、正しく計算することはできませんでした。");
		}

		return  'product_'. $this->id->get() . '.jpg';
	}

	/**
	 * 価格変更
	 * @param int $price
	 */
	public function changePrice(int $price)
	{
		$this->price = $price;
	}
}
