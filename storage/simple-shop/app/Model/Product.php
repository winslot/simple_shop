<?php namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class product
 * @package App\Model
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image_name
 * @property int $price
 * @property Carbon $registered_at
 */
class Product extends Model
{
	protected $connection = "master";
	public $timestamps = true;

	protected $dates = [
		'registered_at'
	];
}