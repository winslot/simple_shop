<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Product as ProductModel;

class InsertDefaultProducts extends Command
{
	protected $signature = 'insert_default_products';
	protected $description = 'レビュー用デフォルト商品を作成する。';

	public function handle()
	{
		ProductModel::insert(['id' => '1', 'name' => '加藤恵フィギュア', 'description' => '冴えない彼女の育てかたメインヒロインの一人のフィギュア', 'price' => '3000']);
		ProductModel::insert([
			'id' => '2',
			'name' => 'めぐみん フィギュア',
			'description' => '我が名はめぐみん！ アークウィザードを生業とし、最強の攻撃魔法〈 爆裂魔法 〉を操りし者！',
			'price' => '8000']
		);
		ProductModel::insert(['id' => '3', 'name' => 'ポケモンカードモルペコ', 'description' => 'ポケモンカード：マリィの仲間のモルペコ', 'price' => '300']);
		ProductModel::insert(['id' => '4', 'name' => 'ククリ', 'description' => '魔法陣グルグル ククリ 塗装済み可動フィギュア', 'price' => '7250']);
		ProductModel::insert([
			'id' => '5',
			'name' => 'あの花 【Blu-ray BOX 完全生産限定版】',
			'description' => 'あの日見た花の名前を僕達はまだ知らない。【Blu-ray BOX 完全生産限定版】',
			'price' => '20000']
		);
	}
}