<?php namespace App\Http\Controllers\Auth;

use App\DomainModel\User\User;
use App\DomainModel\User\UserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

/**
 * Class UserController
 * @package App\Http\Controllers\Auth
 */
class UserController extends Controller
{
	/**
	 * ユーザー情報を返す。 ログインしてないならゲストを返す。
	 * @return JsonResponse
	 */
	public function getUser()
	{
		if (\Auth::check()) {
			/** @var User $user */
			$user = app(UserRepository::class)->getLoginUser();
			return response()->json([
				'name' => $user->getUserName(),
				'is_login' => true
			], 200);
		} else {
			return response()->json([
				'name' => '名も無きゲスト',
				'is_login' => false
			], 200);
		}
	}
}