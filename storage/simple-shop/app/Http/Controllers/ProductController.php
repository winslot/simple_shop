<?php namespace App\Http\Controllers;

use App\DomainModel\Product\Product;
use App\DomainModel\Product\ProductId;
use App\DomainModel\Product\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	/**
	 * 全商品を返す
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function searchProducts(Request $request)
	{
		$keyword = $request->input('keyword');

		/** @var ProductRepository $repository */
		$repository = app(ProductRepository::class);

		if(is_null($keyword) || !isset($keyword) || strlen($keyword) == 0) {
			$products = $repository->all();
		} else {
			$products = $repository->selectByKeyword($keyword);
		}

		return response()->json(
			$products->toArray(),
			200
		);
	}

	/**
	 * 特定商品の情報を返す
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function getProductDetail(Request $request)
	{
		$this->validate($request, [
			'product_id' => 'required|integer',
		]);

		$productId = new ProductId($request->input('product_id'));

		/** @var Product $product */
		$product = app(ProductRepository::class)->find($productId);

		if (is_null($product)) {
			throw new \Exception("存在してない商品を取得しようとしている。 商品ID : " . $productId->get());
		}

		return response()->json([
			'id' => $product->getId()->get(),
			'name' => $product->getName(),
			'description' => $product->getDescription(),
			'price' => $product->getPrice(),
			'image_name' =>$product->getImageName()
		], 200);
	}
}