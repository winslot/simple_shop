<?php namespace App\Http\Controllers\Admin;

use App\DomainModel\Product\ProductId;
use App\Http\Controllers\Controller;
use App\DomainModel\Product\ProductRepository;
use Illuminate\Http\Request;
use App\DomainModel\Product\Product;
use Illuminate\View\View;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * 商品管理コントローラー
 * Class ProductManagementController
 * @package App\Http\Controllers\Admin
 */
class ProductManagementController extends Controller
{
	use ValidatesRequests;

	/**
	 * 画像を保存するパス。
	 * app/public/images
	 */
	const IMAGE_PATH = 'images/';

	/**
	 * 全商品を返す
	 * @param Request $request
	 * @return View
	 */
	public function searchProducts(Request $request)
	{
		/** @var ProductRepository $repository */
		$repository = app(ProductRepository::class);
		$products = $repository->all();

		return view('admin.product_list_page')->with('products', $products->getAll());
	}

	/**
	 * 詳細ページを取得する
	 * @param Request $request
	 * @return View
	 * @throws \Exception
	 */
	public function getProductDetailPage(Request $request)
	{
		$productId = new ProductId($request->input('product_id'));
		$product = app(ProductRepository::class)->find($productId);

		if (is_null($product)) {
			throw new \Exception("存在してない商品の詳細画面に遷移しようとしている。 商品ID: " . $productId->get());
		}

		return view('admin.product_detail_page')->with('product', $product);
	}

	/**
	 * 商品登録ページ
	 * @return View
	 */
	public function getRegisterProductPage()
	{
		return view('admin.product_register_page');
	}

	/**
	 * 商品登録
	 * @param Request $request
	 * @return View
	 * @throws \Exception
	 */
	public function registerProduct(Request $request)
	{
		$this->validate($request, [
			'name' => 'required|string|max:30',
			'description' => 'required|string|max:255',
			'price' => 'required|integer',
			'image' => 'required|image|max:2048'
		]);

		$name = $request->input('name');
		$description = $request->input('description');
		$price = $request->input('price');

		$product = Product::registerProduct($name, $description ,$price);
		app(ProductRepository::class)->persist($product);

		$request->file('image')->move(self::IMAGE_PATH, $imageName = $product->getImageName());;

		return redirect('/admin/product_list_page');
	}

	/**
	 * 商品編集
	 * @param Request $request
	 * @return View
	 * @throws \Exception
	 */
	public function editProduct(Request $request)
	{
		//TODO トランザクションを実装する？
		$this->validate($request, [
			'product_id' => 'required|integer',
			'name' => 'required|string|max:30',
			'description' => 'required|string|max:255',
			'price' => 'required|integer',
			'image' => 'image|max:2048'
		]);

		$productId = new ProductId($request->input('product_id'));

		/** @var ProductRepository $repository */
		$repository = app(ProductRepository::class);
		$product = $repository->find($productId);

		if (is_null($product)) {
			throw new \Exception("存在してない商品の情報を変更しようとしている。 商品ID: " . $productId->get());
		}

		$imageFile = $request->file('image');
		if (!is_null($imageFile)) {
			$imageFile->move(self::IMAGE_PATH, $imageName = $product->getImageName());
		}

		$product->rename($request->input('name'));
		$product->updateDescription($request->input('description'));
		$product->changePrice($request->input('price'));
		$repository->persist($product);

		return redirect('/admin/product_list_page');
	}

	/**
	 * 商品を削除する
	 * @param Request $request
	 * @return View
	 */
	public function deleteProduct(Request $request)
	{
		$productId = new ProductId($request->input('product_id'));
		app(ProductRepository::class)->delete($productId);

		return redirect('/admin/product_list_page');
	}
}
