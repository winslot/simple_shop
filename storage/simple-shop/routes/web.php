<?php

/**
 * 管理画面
 */
Route::get('/admin/product_list_page', 'Admin\ProductManagementController@searchProducts');
Route::get('/admin/product_register_page', 'Admin\ProductManagementController@getRegisterProductPage');
Route::get('/admin/product_detail_page', 'Admin\ProductManagementController@getProductDetailPage');
Route::post('/admin/edit_product', 'Admin\ProductManagementController@editProduct');
Route::post('/admin/register_product', 'Admin\ProductManagementController@registerProduct');
Route::post('/admin/delete_product', 'Admin\ProductManagementController@deleteProduct');

/**
 * 商品管理
 */
Route::get('/products', 'ProductController@searchProducts');
Route::get('/product', 'ProductController@getProductDetail');

/**
 * ユーザー関連
 */
Route::get('/user', 'Auth\UserController@getUser');

/**
 * 公式が準備した機能
 */
Auth::routes();
