/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Init Menu
4. Init Thumbnail
5. Init Quantity
6. Init Star Rating
7. Init Favorite
8. Init Tabs



******************************/

jQuery(document).ready(function($)
{
	"use strict";

	/* 

	1. Vars and Inits

	*/

	var header = $('.header');
	var topNav = $('.top_nav')
	var hamburger = $('.hamburger_container');
	var menu = $('.hamburger_menu');
	var menuActive = false;
	var hamburgerClose = $('.hamburger_close');
	var fsOverlay = $('.fs_menu_overlay');

	setHeader();

	$(window).on('resize', function()
	{
		setHeader();
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});

	//URL?product_id=x のパターンのみ対応
	var productId = location.search.substr(1).split("=")[1];

	$.ajax({
		type: 'get',
		url: "http://0.0.0.0:8001/product?product_id="+productId,
		cache: false,
		data: {'product_id': productId},
		async:true,
		success: function (response) {
			insertProductContentHTML(response);
			initMenu();
			initThumbnail();
			initQuantity();
			initStarRating();
			initFavorite();
			initTabs();
			initUserContent();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			//エラー処理
		},            
		statusCode: {
		404: function() {
			alert("page not found");
		}
		}
	});

	/**
	 * 商品内容をインサートする
	 * @param productResponse product: [id, name, description, price, image_name]
	 */
	function insertProductContentHTML(productResponse)
	{
		var productContentHTML = "";
		productContentHTML = productContentHTML.concat('<div class="col-lg-7">');
		productContentHTML = productContentHTML.concat('<div class="single_product_pics">');
		productContentHTML = productContentHTML.concat('<div class="row">');
		productContentHTML = productContentHTML.concat('<div class="col-lg-9 image_col order-lg-2 order-1">');
		productContentHTML = productContentHTML.concat('<div class="single_product_image">');
		productContentHTML = productContentHTML.concat('<div class="single_product_image_background" style="background-image:url('+ productResponse.image_name +')"></div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('<div class="col-lg-5">');
		productContentHTML = productContentHTML.concat('<div class="product_details">');
		productContentHTML = productContentHTML.concat('<div class="product_details_title">');
		productContentHTML = productContentHTML.concat('<h2>'+ productResponse.name +'</h2>');
		productContentHTML = productContentHTML.concat('<p>'+ productResponse.description +'</p>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('<div class="product_price">'+ (productResponse.price) +'¥</div>');
		productContentHTML = productContentHTML.concat('<div class="quantity d-flex flex-column flex-sm-row align-items-sm-center">');
		productContentHTML = productContentHTML.concat('<span>Quantity:</span>');
		productContentHTML = productContentHTML.concat('<div class="quantity_selector">');
		productContentHTML = productContentHTML.concat('<span class="minus"><i class="fa fa-minus" aria-hidden="true"></i></span>');
		productContentHTML = productContentHTML.concat('<span id="quantity_value">1</span>');
		productContentHTML = productContentHTML.concat('<span class="plus"><i class="fa fa-plus" aria-hidden="true"></i></span>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('<div class="red_button add_to_cart_button"><a href="#">add to cart</a></div>');
		productContentHTML = productContentHTML.concat('<div class="product_favorite d-flex flex-column align-items-center justify-content-center"></div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('</div>');
		productContentHTML = productContentHTML.concat('</div>');

		document.getElementById("productContent").innerHTML = productContentHTML;
	}

	/**
	 * ユーザー情報を入れる
	 */
	function initUserContent()
	{
		$.ajax({
			type: 'get',
			url: "http://0.0.0.0:8001/user",
			cache: false,
			data: {},
			async:false,
			success: function (response) {
				console.log(response);
				if(response.is_login) {
					var content = "";
					content = content.concat('<h5>Hi, '+response.name+'</h5>');
					content = content.concat('<form method="POST" action="/logout">');
					content = content.concat('<button class="btn aqua-gradient btn-rounded btn-sm my-0" type="submit">ログアウト</button>');
					content = content.concat('</form>');
				} else {
					var content = "";
					content = content.concat('<h5>Hi, '+response.name+'</h5>');
					content = content.concat('<a href="login"><button type="button" class="btn aqua-gradient btn-rounded btn-sm my-0" >ログイン</button></a>');
					content = content.concat('<a href="register"><button type="button" class="btn aqua-gradient btn-rounded btn-sm my-0" >登録</button></a>');
				}

				document.getElementById("userContent").innerHTML = content;
			},
			error: function (xhr, ajaxOptions, thrownError) {
				//エラー処理
			},            
			statusCode: {
			404: function() {
				alert("page not found");
			}
			}
		});
	}


	/* 

	2. Set Header

	*/

	function setHeader()
	{
		if(window.innerWidth < 992)
		{
			if($(window).scrollTop() > 100)
			{
				header.css({'top':"0"});
			}
			else
			{
				header.css({'top':"0"});
			}
		}
		else
		{
			if($(window).scrollTop() > 100)
			{
				header.css({'top':"-50px"});
			}
			else
			{
				header.css({'top':"0"});
			}
		}
		if(window.innerWidth > 991 && menuActive)
		{
			closeMenu();
		}
	}

	/* 

	3. Init Menu

	*/

	function initMenu()
	{
		if(hamburger.length)
		{
			hamburger.on('click', function()
			{
				if(!menuActive)
				{
					openMenu();
				}
			});
		}

		if(fsOverlay.length)
		{
			fsOverlay.on('click', function()
			{
				if(menuActive)
				{
					closeMenu();
				}
			});
		}

		if(hamburgerClose.length)
		{
			hamburgerClose.on('click', function()
			{
				if(menuActive)
				{
					closeMenu();
				}
			});
		}

		if($('.menu_item').length)
		{
			var items = document.getElementsByClassName('menu_item');
			var i;

			for(i = 0; i < items.length; i++)
			{
				if(items[i].classList.contains("has-children"))
				{
					items[i].onclick = function()
					{
						this.classList.toggle("active");
						var panel = this.children[1];
					    if(panel.style.maxHeight)
					    {
					    	panel.style.maxHeight = null;
					    }
					    else
					    {
					    	panel.style.maxHeight = panel.scrollHeight + "px";
					    }
					}
				}	
			}
		}
	}

	function openMenu()
	{
		menu.addClass('active');
		// menu.css('right', "0");
		fsOverlay.css('pointer-events', "auto");
		menuActive = true;
	}

	function closeMenu()
	{
		menu.removeClass('active');
		fsOverlay.css('pointer-events', "none");
		menuActive = false;
	}

	/* 

	4. Init Thumbnail

	*/

	function initThumbnail()
	{
		if($('.single_product_thumbnails ul li').length)
		{
			var thumbs = $('.single_product_thumbnails ul li');
			var singleImage = $('.single_product_image_background');

			thumbs.each(function()
			{
				var item = $(this);
				item.on('click', function()
				{
					thumbs.removeClass('active');
					item.addClass('active');
					var img = item.find('img').data('image');
					singleImage.css('background-image', 'url(' + img + ')');
				});
			});
		}	
	}

	/* 

	5. Init Quantity

	*/

	function initQuantity()
	{
		if($('.plus').length && $('.minus').length)
		{
			var plus = $('.plus');
			var minus = $('.minus');
			var value = $('#quantity_value');

			plus.on('click', function()
			{
				var x = parseInt(value.text());
				value.text(x + 1);
			});

			minus.on('click', function()
			{
				var x = parseInt(value.text());
				if(x > 1)
				{
					value.text(x - 1);
				}
			});
		}
	}

	/* 

	6. Init Star Rating

	*/

	function initStarRating()
	{
		if($('.user_star_rating li').length)
		{
			var stars = $('.user_star_rating li');

			stars.each(function()
			{
				var star = $(this);

				star.on('click', function()
				{
					var i = star.index();

					stars.find('i').each(function()
					{
						$(this).removeClass('fa-star');
						$(this).addClass('fa-star-o');
					});
					for(var x = 0; x <= i; x++)
					{
						$(stars[x]).find('i').removeClass('fa-star-o');
						$(stars[x]).find('i').addClass('fa-star');
					};
				});
			});
		}
	}

	/* 

	7. Init Favorite

	*/

	function initFavorite()
	{
		if($('.product_favorite').length)
		{
			var fav = $('.product_favorite');

			fav.on('click', function()
			{
				fav.toggleClass('active');
			});
		}
	}

	/* 

	8. Init Tabs

	*/

	function initTabs()
	{
		if($('.tabs').length)
		{
			var tabs = $('.tabs li');
			var tabContainers = $('.tab_container');

			tabs.each(function()
			{
				var tab = $(this);
				var tab_id = tab.data('active-tab');

				tab.on('click', function()
				{
					if(!tab.hasClass('active'))
					{
						tabs.removeClass('active');
						tabContainers.removeClass('active');
						tab.addClass('active');
						$('#' + tab_id).addClass('active');
					}
				});
			});
		}
	}
});