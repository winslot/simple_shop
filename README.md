【環境構築手順書】
    Macの構築手順です。
    Git: https://mistua@bitbucket.org/winslot/simple_shop.git
   
    1. dockerをダウンロードし、インストールする、起動する
        ○ 公式ページ: https://hub.docker.com/editions/community/docker-ce-desktop-mac/
        ○ ダウンロードURL: https://download.docker.com/mac/stable/Docker.dmg
        ○ docker --version
    2. docker-composeをinstallする
        ○ 公式ページ: https://docs.docker.com/compose/install/
        ○ Macの場合、docker.dmgをインストールする時にもうすでにインストールされたため、
            インストールは不要となります。
        ○ docker-compose --version
    3. Gitからプロジェクトをダウンロードする
        ○ git clone https://mistua@bitbucket.org/winslot/simple_shop.git
    4. git branchをreviewに切り替える
        ○ git checkout review
        ○ ライブラリをダウンロードするには時間かかるため、ライブラリの内容を直接reviewに
            入れました。(composer update)
    5. 環境を構築する
        ○ cd simple_shop
        ○ docker-compose up --build -d
    6. Mysql初期化を待つ
        ○ 10秒〜30秒
    7. ショップの初期化を実行する。
        ○ docker-compose exec php-fpm-service sh /var/project/simple-shop/setup_docker.sh
    8. 構築完了
【機能】

    1. 商品一覧
        ○ http://0.0.0.0:8001/product_list.html
    2. 管理画面TOP
        ○ http://0.0.0.0:8001/admin/product_list_page
        

![Scheme](readme_image/top_page.png)
![Scheme](readme_image/admin_page.png)        
        
